# README #

### What is this repository for? ###

* This repo illustrates code recipes for opennlp and other toolkits

### How do I get set up? ###

* Check out project
* Project requires Maven to compile
* Open in Eclipse, Compile and Run tests

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines