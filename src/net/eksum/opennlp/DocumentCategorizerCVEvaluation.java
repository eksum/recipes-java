package net.eksum.opennlp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import opennlp.tools.doccat.BagOfWordsFeatureGenerator;
import opennlp.tools.doccat.DoccatEvaluationMonitor;
import opennlp.tools.doccat.DoccatFactory;
import opennlp.tools.doccat.DoccatModel;
import opennlp.tools.doccat.DocumentCategorizerEvaluator;
import opennlp.tools.doccat.DocumentCategorizerME;
import opennlp.tools.doccat.DocumentSample;
import opennlp.tools.doccat.DocumentSampleStream;
import opennlp.tools.ml.AbstractTrainer;
import opennlp.tools.util.MarkableFileInputStreamFactory;
import opennlp.tools.util.ObjectStream;
import opennlp.tools.util.PlainTextByLineStream;
import opennlp.tools.util.TrainingParameters;
import opennlp.tools.util.eval.CrossValidationPartitioner;
import opennlp.tools.util.eval.CrossValidationPartitioner.TrainingSampleStream;

/**
 * Code recipe that illustrates Cross Validation Evaluation for Document
 * Categorizer
 * 
 * @author sumukh
 *
 */
public class DocumentCategorizerCVEvaluation {
	public static void main(String[] args) throws FileNotFoundException, IOException {
		DocumentCategorizerCVEvaluation cvEvaluation = new DocumentCategorizerCVEvaluation();
		cvEvaluation.showRecipe();
	}

	private void showRecipe() throws FileNotFoundException, IOException {
		DoccatModel model = null;
		// version < 1.6.0
		// ObjectStream<String> lineStream = new PlainTextByLineStream(
		//		new FileInputStream("data/questionset.train").getChannel(), "UTF-8");

		ObjectStream<String> lineStream = new PlainTextByLineStream(
				new MarkableFileInputStreamFactory(new File("data/questionset.train")), "UTF-8");

		ObjectStream<DocumentSample> sampleStream = new DocumentSampleStream(lineStream);
		CrossValidationPartitioner<DocumentSample> cvPartitioner = new CrossValidationPartitioner<DocumentSample>(
				sampleStream, 10);

		BinaryClassificationMetrics m = new BinaryClassificationMetrics("1", "0");

		while (cvPartitioner.hasNext()) {
			TrainingSampleStream<DocumentSample> train = cvPartitioner.next();

			// version < 1.6.0
			// model = DocumentCategorizerME.train("en", train, 1, 100, new
			// BagOfWordsFeatureGenerator());

			// version >= 1.6.0
			TrainingParameters params = new TrainingParameters();
			params.put(AbstractTrainer.CUTOFF_PARAM, "1");
			params.put(AbstractTrainer.ITERATIONS_PARAM, "100");
			model = DocumentCategorizerME.train("en", train, params, new DoccatFactory());

			DocumentCategorizerME docCat = new DocumentCategorizerME(model);
			ObjectStream<DocumentSample> test = train.getTestSampleStream();
			DocumentSample testSample;
			while ((testSample = test.read()) != null) {
				String[] text = testSample.getText();
				String expected = testSample.getCategory();
				String returned = docCat.getBestCategory(docCat.categorize(text));
				m.update(expected, returned);
			}
		}
		System.out.println("Precision:\t" + m.getPrecision());
		System.out.println("Recall:\t\t" + m.getRecall());
		System.out.println("Fscore:\t\t" + m.getFScore());

		m.printConfusionMatrix();
	}
}
