package net.eksum.opennlp;

import java.text.DecimalFormat;

class BinaryClassificationMetrics {
	private static final DecimalFormat FORMAT = new DecimalFormat("#.####");
	private String trueValue;
	private String falseValue;
	private int truePositive = 0;
	private int falsePositive = 0;
	private int trueNegative = 0;
	private int falseNegative = 0;
	private int actualTrue = 0;

	public BinaryClassificationMetrics(String trueValue, String falseValue) {
		this.trueValue = trueValue;
		this.falseValue = falseValue;
	}

	public String getFScore() {
		double p = getPrecisionRaw();
		double r = getRecallRaw();
		if (p + r == 0) {
			return "0";
		}
		return FORMAT.format(2.0 * p * r / (p + r));
	}

	public String getRecall() {
		return FORMAT.format(getRecallRaw());
	}

	private double getRecallRaw() {
		if (actualTrue == 0) return 1;
		return truePositive * 1.0 / actualTrue;
	}

	public String getPrecision() {
		return FORMAT.format(getPrecisionRaw());
	}

	private double getPrecisionRaw() {
		int returnedPositive = truePositive + falsePositive;
		if (returnedPositive == 0) {
			 return 0;
		}
		return truePositive * 1.0 / returnedPositive;
	}

	public int getTruePositive() {
		return truePositive;
	}

	public int getTrueNegative() {
		return trueNegative;
	}

	public int getFalsePositive() {
		return falsePositive;
	}

	public int getFalseNegative() {
		return falseNegative;
	}

	public void update(String expected, String returned) {
		boolean e = expected.equals(trueValue);
		if (!e && !expected.equals(falseValue)) {
			throw new IllegalArgumentException("Unknown class value " + expected);
		}
		boolean r = returned.equals(trueValue);
		if (!r && !returned.equals(falseValue)) {
			throw new IllegalArgumentException("Unknown class value " + returned);
		}
		if (e) {
			actualTrue++;
		}
		if (e && r) {
			truePositive++;
		} else if (e && !r) {
			falseNegative++;
		} else if (!e && r) {
			falsePositive++;
		} else {
			trueNegative++;
		}
	}
	
	public void printConfusionMatrix() {
		System.out.println("\tFalse\tTrue <- Classified as");
		System.out.println("False\t" + getTrueNegative() + "\t" + getFalsePositive());
		System.out.println("True\t" + getFalseNegative() + "\t" + getTruePositive());
		System.out.println("^ Actual");
	}
}