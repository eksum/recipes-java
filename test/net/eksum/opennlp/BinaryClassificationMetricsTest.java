package net.eksum.opennlp;

import junit.framework.TestCase;

public class BinaryClassificationMetricsTest extends TestCase {
	public void testEmpty() throws Exception {
		BinaryClassificationMetrics m = new BinaryClassificationMetrics("1", "0");
		assertMetrics("0", "1", "0", 0, 0, 0, 0, m);
	}
	
	public void testSinglePositiveInstance() throws Exception {
		BinaryClassificationMetrics m = new BinaryClassificationMetrics("1", "0");
		m.update("1", "1");
		assertMetrics("1", "1", "1", 1, 0, 0, 0, m);
	}
	
	public void testOneTruePosAndOneTrueNegInstance() throws Exception {
		BinaryClassificationMetrics m = new BinaryClassificationMetrics("1", "0");
		m.update("1", "1");
		m.update("0", "0");
		assertMetrics("1", "1", "1", 1, 1, 0, 0, m);
	}
	
	public void testOneEach() throws Exception {
		BinaryClassificationMetrics m = new BinaryClassificationMetrics("1", "0");
		m.update("1", "1");
		m.update("0", "0");
		m.update("1", "0");
		m.update("0", "1");
		assertMetrics("0.5", "0.5", "0.5", 1, 1, 1, 1, m);
	}
	
	public void testMostlyPositive() throws Exception {
		BinaryClassificationMetrics m = new BinaryClassificationMetrics("1", "0");
		m.update("1", "1");
		m.update("1", "1");
		m.update("1", "1");
		m.update("1", "1");
		m.update("1", "0");
		m.update("0", "1");
		m.update("0", "0");
		assertMetrics("0.8", "0.8", "0.8", 4, 1, 1, 1, m);
	}

	private void assertMetrics(String precision, String recall, String fScore, int truePositive, int trueNegative,
			int falsePositive, int falseNegative, BinaryClassificationMetrics m) {
		assertEquals(precision, m.getPrecision());
		assertEquals(recall, m.getRecall());
		assertEquals(fScore , m.getFScore());
		assertEquals(truePositive, m.getTruePositive());
		assertEquals(trueNegative, m.getTrueNegative());
		assertEquals(falsePositive, m.getFalsePositive());
		assertEquals(falseNegative, m.getFalseNegative());
	}
}
